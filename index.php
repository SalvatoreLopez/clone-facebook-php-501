<?php include 'modulos/config.php';?>
<?php $titulo = "Registro"; $mostrarMenu = false; include 'modulos/componentes/header.php'; ?>
<div class="container-fluid">
    <div class="row" style="margin-top: 3rem;">
        <div class="col-6 text-center p-2">
             <p class="font-weight-bold h5">Inicios de sesión recientes</p>
                <p class="font-weight-bold h5">Haz clic en tu foto o agrega una cuenta.</p>
            <a  href="<?php echo $raiz."modulos/paginas/pagina-principal.php";?>" title="Eduardo Mejía" class="_1gbd" role="button" id="u_0_i"><img src="img/Eduardo-Mejia.jpg" alt="" aria-label="Eduardo Mejía" role="$mostrarMenu" style="width:160px;height:160px"><div class="_1gaz _c24 _50f6">Eduardo</div></form></a>
        </div>
        <div class="col-6 pl-4 pr-4 pb-4" >
            <h1 class="font-weight-bold h2">Abre una cuenta</h1>
            <h2 class=" h6">Es gratis y lo será siempre.</h2>
            <!--action="modulos/php/registrar-usuario.php" method="POST"-->
            <form class="needs-validation" novalidate id="userRegister">
                <div class="row">
                    <div class="col-6">
                        <input type="text" style="background: white;border-radius: 3px; border: 1px solid #bdc7d8;" class="form-control" name="nombre"  placeholder="Nombre" required>
                        <div class="invalid-tooltip">
                            por favor ingresa tu nombre!
                        </div>
                    </div>
                    <div class="col-6">
                        <input type="text" style="background: white;border-radius: 3px; border: 1px solid #bdc7d8;" class="form-control" name="apellido"  placeholder="Apellido" required>
                        <div class="invalid-tooltip">
                            por favor ingresa tu apellido!
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <input type="text" class="form-control  mt-3" style="background: white;border-radius: 3px; border: 1px solid #bdc7d8;" name="correoOTelefono" placeholder="Número celular o correo electrónico" required>
                        <div class="invalid-tooltip">
                            por favor ingresa un correo o mail!
                        </div>
                    </div>
                    <div class="col-12">
                        <input type="password" class="form-control  mt-3" style="background: white;border-radius: 3px; border: 1px solid #bdc7d8;" name="contrasenia" placeholder="Contraseña nueva" required>
                        <div class="invalid-tooltip">
                            por favor ingresa tu password!
                        </div>
                    </div>
                </div>
                <h3 class="h6 mt-3">Fecha de nacimiento</h3>
                <div class="row">
                    <div class="col-4">
                        <select class="custom-select mt-2" name="dia"  required>
                            <option value="">Día</option>
                            <?php for($i=0; $i<31; $i++): ?>
                                <option value="<?php echo ($i+1);?>"><?php echo ($i+1);?></option>
                            <?php endfor;?>
                        </select>
                        <div class="invalid-tooltip">
                            por favor ingresa dia de nacimiento!
                        </div>
                    </div>
                    <div class="col-4">
                        <?php
                            $meses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];
                        ?>
                        <select class="custom-select mt-2" name="mes" required>
                            <option value="">Mes</option>
                            <?php for($i=0; $i<12; $i++): ?>
                                <option value="<?php echo ($i+1);?>"><?php echo $meses[$i];?></option>
                            <?php endfor;?>
                        </select>
                        <div class="invalid-tooltip">
                            por favor ingresa mes de nacimiento!
                        </div>
                    </div>
                    <div class="col-4">
                        <select name="anio"  class="custom-select mt-2" required>
                            <option value="">Año</option>
                            <?php $obtenerAnio = date('Y'); ?>
                            <?php for( $i = $obtenerAnio ; $i >= 1905 ; $i--): ?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php endfor;?>
                        </select>
                        <div class="invalid-tooltip">
                            por favor ingresa año de nacimiento!
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <select class="custom-select mt-3"  name="genero"  required>
                            <option value="">Genero</option>
                            <option value="1">Mujer</option>
                            <option value="2">Hombre</option>
                        </select>
                        <div class="invalid-tooltip">
                            por favor ingresa tu genero!
                        </div>
                    </div>
                </div>
                <p class="mb-0 mt-2">Al hacer clic en "Registrarte", aceptas nuestras Condiciones, la Política de datos y la Política de cookies. Es posible que te enviemos notificaciones por SMS, que puedes desactivar cuando quieras.</p>
                <button class="mt-2 btn btn-raised btn-success ">Registrar</button>
            </form>
        </div>
    </div>
</div>
<script src="js/index.js"></script>
<?php include 'modulos/componentes/footer.php'; ?>