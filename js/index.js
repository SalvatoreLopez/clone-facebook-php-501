(() => {
    'use strict';
    window.addEventListener('load', () => {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      var userRegister = document.getElementById("userRegister");
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, (form) => {
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            event.stopPropagation();
            if(form.checkValidity()) {
                let dataForm = new FormData(userRegister);
                fetch('modulos/php/registrar-usuario.php',{
                    method : 'POST',
                    body : dataForm
                })
                .then((respuesta)=>respuesta.json())
                .then((data)=>{
                    console.log(data);
                })
                .catch((error)=>console.log(error))
            }
            form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();