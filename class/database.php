<?php
    class database{
        //creamos las variables de configuración
        private $host = "localhost";
        private $usuario = "root";
        private $contrasenia = "";
        private $nombreDeLaBaseDeDatos = "dbclonefacebook";

        //creamos el objeto que nos proveera la conexión desde cualquier clase
        public function obtenerConexion(){
            $conexion = new PDO("mysql:host=$this->host;dbname=$this->nombreDeLaBaseDeDatos;charset=utf8;",$this->usuario,$this->contrasenia);
            //$conexion->setAttribute(PDO::ATTR_ERROMODE,PDO::ERROMODE_EXCEPTION);
            return $conexion;
        }
    }