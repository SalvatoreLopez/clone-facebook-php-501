<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $titulo; ?></title>
    <link rel="stylesheet" href="<?php echo $directorioPadre; ?>css/all.css">
    <link rel="stylesheet" href="<?php echo $directorioPadre; ?>css/bootstrap-material-design.css">
    <link rel="stylesheet" href="<?php echo $directorioPadre; ?>css/red-social.css">
    <link rel="shortcut icon" href="<?php echo $directorioPadre; ?>img/facebook.png" type="image/x-icon">
</head>
<body>
    <main class="bg-gris">
        <?php  if( $mostrarMenu ) : ?>
            <ul class="nav menu-navegacion bg-azul justify-content-center fixed-top">
                <li class="busqueda">
                    <div class="input-group mt-1">
                        <div class="input-group-prepend logotipo">
                            <span class="input-group-text"><i class="fab fa-facebook-square"></i></span>
                        </div>
                        <input type="text" class="form-control input-busqueda" placeholder="Buscar">
                        <div class="input-group-append icono-busqueda">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white mt-1 font-family" href="#"> <img class="avatar mr-1" src="img/Kyloren.jpg" alt="avatar-usuario"> Mejia</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white mt-1 font-family" href="#">Inicio</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-white mt-1 font-family" href="#">Crear</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-white mt-1 font-family" href="#"><i class="fas fa-user-friends"></i></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-white mt-1 font-family" href="#"><i class="fab fa-facebook-messenger"></i></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-white mt-1 font-family" href="#"><i class="fas fa-bell"></i></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-white mt-1 font-family" href="#"><i class="fas fa-question-circle"></i></a>
                </li>

                <li class="nav-item">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle mas-opciones" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width: 13rem;">
                            <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i> Configuración</a>
                            <a class="dropdown-item" href="#"><i class="fas fa-sign-out-alt mr-2"></i> Cerrar Sesión</a>
                        </div>
                    </div>
                </li>
            </ul>
            <?php else:?>
                <div class="container-fluid  bg-azul">
                    <div class="row">
                        <div class="col-5 text-center">
                            <p class="text-white h2 font-weight-bold mb-0 mt-2">Facebook</p>
                        </div>
                        <div class="col-7  mt-2">
                            <form action="modulos/php/login.php" method="POST">
                                <div class="row">
                                    <div class="col-5">
                                        <input type="text" style="background: white;" class="form-control" name="telefono"  placeholder="Correo o telefono">
                                    </div>
                                    <div class="col-5">
                                        <input type="password" style="background: white;" class="form-control" name="password"  placeholder="Contraseña">
                                    </div>
                                    <div class="col-2">
                                        <button  class="btn btn-raised btn-success">Entrar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        <?php endif;?>