<?php include '../../modulos/config.php';?>
<?php $titulo = "Registro"; $mostrarMenu = true; include '../../modulos/componentes/header.php'; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="portada" style="background: url(../../img/2.jpg);
                    height: 22rem;
                    width: 100%;
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: center;">

                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                Presentación
                            </div>
                            <div class="card-body">
                                <p class="mb-0 card-text">
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Adipisci voluptatibus facere, enim nam fuga eveniet dignissimos, minus sunt vel numquam voluptatum repellendus obcaecati error ad qui, veniam expedita nesciunt eligendi facilis voluptatem! Nostrum nulla molestiae eveniet temporibus repudiandae aperiam similique.
                                </p>
                            </div>
                        </div>

                        <div class="card mt-2">
                            <div class="card-header">
                               <label>Fotos</label>
                               <a href="" class="float-right">Agregar Foto</a> 
                            </div>
                            <div class="card-body p-1">
                               <div class="row">
                                    <?php for($i = 0 ; $i<9; $i++): ?>
                                        <div class="col-md-4">
                                            <img class="img-fluid mb-1" src="../../img/2.jpg" alt="">
                                        </div>
                                    <?php endfor; ?>
                               </div>
                            </div>
                        </div>

                        <div class="card mt-2">
                            <div class="card-header">
                               <label>Amigos</label>
                               <a href="" class="float-right">Agregar Amigo</a> 
                            </div>
                            <div class="card-body p-1">
                               <div class="row">
                                    <?php for($i = 0 ; $i<9; $i++): ?>
                                        <div class="col-md-4">
                                            <img class="img-fluid mb-1" src="../../img/2.jpg" alt="">
                                        </div>
                                    <?php endfor; ?>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                    <?php for($i = 0; $i<110 ; $i++):?>
                        <div class="card ">
                            <img src="../../img/2.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                <div class="row">
                                    <div class="col-5">
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Comentario">
                                    </div>
                                    <div class="col-7">
                                        <div class="btn-group float-right" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-secondary font-family"><i class="far fa-thumbs-up"></i> Me gusta</button>
                                            <button type="button" class="btn btn-secondary font-family"><i class="far fa-comments"></i> Comentar</button>
                                            <button type="button" class="btn btn-secondary font-family"  data-toggle="modal" data-target="#exampleModal<?php echo $i;?>"><i class="fas fa-share"></i> Compartir</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title <?php echo $i+1;?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    ...
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Publicar</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    <?php endfor; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include '../../modulos/componentes/footer.php'; ?>