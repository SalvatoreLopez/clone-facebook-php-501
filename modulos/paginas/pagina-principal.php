<?php include 'modulos/config.php';?>
 <?php $titulo = "pagina principal"; $mostarMenu = true; include 'modulos/componentes/header.php'; ?>
        <div class=" container">
            <div class="row" style="margin-top: 3rem; ">
                <div class="col-2">
                    <ul class="nav flex-column menu-left">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Eduardo Mejia</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Link</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Link</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                        </li>
                    </ul>
                </div>
                <div class="col-8">
                    <?php for($i = 0; $i<110 ; $i++):?>
                        <div class="card mt-3 ">
                            <img src="img/2.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                <div class="row">
                                    <div class="col-5">
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Comentario">
                                    </div>
                                    <div class="col-7">
                                        <div class="btn-group float-right" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-secondary font-family"><i class="far fa-thumbs-up"></i> Me gusta</button>
                                            <button type="button" class="btn btn-secondary font-family"><i class="far fa-comments"></i> Comentar</button>
                                            <button type="button" class="btn btn-secondary font-family"  data-toggle="modal" data-target="#exampleModal<?php echo $i;?>"><i class="fas fa-share"></i> Compartir</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title <?php echo $i+1;?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    ...
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Publicar</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="col-2">Historias</div>
            </div>
        </div>
    <?php include 'modulos/componentes/footer.php'; ?>